//* JS05: Print all elements of an array using all 3 different for loops

let array = [45, 546, 7879, 3148];
console.log(array);

//?for loop

for (i = 0; i < array.length; i++)
    console.log(array[i]);


//? for in loop

for (let i in array) {
    console.log(i, array[i]);
}


//? for of loop

for (let value of array) {
    console.log(value)
}