
class Account {
    constructor(
        accountNumber, holdersName,
        balance) {
        this.accountNumber = accountNumber;
        this.holdersName = holdersName;
        this.balance = balance;
    }
    withdraw(amount) {
        console.log("withdrawing: ", amount);
        if (this.balance >= amount) {
            this.balance = this.balance - amount;
            console.log("Balance after withdraw :", this.balance);
        }
        else
            console.log("Insufficient Balance!");



    }
    deposit(amount) {
        console.log("depositing: ", amount);
        this.balance = this.balance + amount;
        console.log("Balance after deposit :", this.balance);

    }
    print() {
        console.log("Account name : ", this.holdersName);
        console.log(" Balance :", this.balance);
    }
}
class SavingsAccount extends Account {
    constructor(minimumBalance, accountNumber, holdersName, balance) {
        super(accountNumber, holdersName, balance);
        this.minimumBalance = minimumBalance;
    }
    withdraw(amount) {
        console.log("withdrawing from SA: ", amount);
        if (this.minimumBalance <= (this.balance - amount)) {
            this.balance = this.balance - amount;
            console.log("Balance after withdraw:", this.balance);

        }
        else {
            console.log("Insufficient Balance in saving account!")
            console.log(" Minimum balance is :", this.minimumBalance);
        }
        super.print();

    }
}

class CurrentAccount extends Account {
    constructor(overdraftLimit, accountNumber, holdersName, balance) {
        super(accountNumber, holdersName, balance);
        this.overdraftLimit = overdraftLimit;

    }
    withdraw(amount) {
        console.log("withdrawing from CA: ", amount);
        if (amount <= (this.balance + this.overdraftLimit))
            this.balance = this.balance - amount;
        else
            console.log("Insufficient Balance in current account!");
        super.print();
    }
}


class FixedDeposit extends Account {
    constructor() {
        super();
    }
    deposit(amount) {
        console.log(" Cannot Deposit!");

    }
    withdraw(amount) {
        console.log(" Cannot withdraw!")
    }
}

const a = new Account(109090, "Shraddha", 100000);
a.print();
a.withdraw(10);
a.deposit(90);
const sa = new SavingsAccount(50000, 555, "Shraddhaji", 1000000);
sa.withdraw(10);
console.log("Shraddha wants lots of money");
sa.withdraw(999000)
const ca = new CurrentAccount(100000, 777, "CRDIndustries", 10000000000);
ca.withdraw(100);
const fd = new FixedDeposit();
fd.withdraw(7000);
fd.deposit(90);
