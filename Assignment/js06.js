const city = ["Indore", "Nagpur", "Mumbai", "Delhi", "Chennai", "Hyderabad", "Pune", "Patna", "Bengaluru",];
console.log(city);

//? a) Print all the cities where name is either 4 or 5 letters

const val = city.filter(word => word.length == 4 || word.length == 5);
console.log(val);

//b) Sort the cities in ascending order

const ascCity = city.sort();
console.log("cities in ascending order", ascCity);



//c) Check if there are any cities with 6 letters length

const cityWith6 = city.find(word => word.length == 6);
console.log("city with 6 words", cityWith6);

//d) Print the cities in descending order

const descCity = city.sort().reverse();
console.log("cities in descending order", descCity);

//e) Print all city names in FULL CAPITAL
const upCase = city.map(word => word.toUpperCase());
console.log(upCase);