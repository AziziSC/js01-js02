/* JS07 : Create a constructor function called Transaction

function Transaction(transactionNumber, details, amount, drcr) {

Create an array of translations with at least 5 elements
let transArray = [ new Transaction(1, "Details", 1000.00, "dr"), ........ ];
a) Print the count of transactions
b) Print only debit transactions
c) Print the total of cr transactions
d) Create an array which hold transaction numbers which have more than 1000 as amount
e) Calculate difference between cr-dr as total amount
*/

function Transaction(transactionNumber, details, amount, drcr) {

    this.transactionNumber = transactionNumber;
    this.details = details;
    this.amount = amount;
    this.drcr = drcr;

}

//console.log(Transaction.count);

let transArray = [new Transaction(1, "Details", 1000.00, "dr"),
new Transaction(2, "shopping", 354656, "dr"),
new Transaction(3, "eat", 906, "cr"),
new Transaction(4, "hut", 97, "cr"),
new Transaction(5, "yui", 45, "cr")];

//a) Print the count of transactions
console.log(transArray.length);

//b) Print only debit transaction
const drVal = transArray.filter(element => element.drcr == "dr");
console.log(drVal);

const cr = transArray.filter(element => element.drcr == "cr");
const dr = transArray.filter(element => element.drcr == "dr");
console.log("ye cr hai", cr);

//c) Print the total of cr transactions
const reducedCr = cr.reduce((total, element) => total + element.amount, 0);
console.log("total credit ", reducedCr);

// d) Create an array which hold transaction numbers which have more than 1000 as amount
const amountArr = transArray.filter(element => element.amount >= 1000);
console.log("transaction numbers which have more than 1000 as amount:", amountArr);

//e) Calculate difference between cr-dr as total amount
const reducedDr = dr.reduce((total, element) => total + element.amount, 0);
console.log("total debit ", reducedDr);

const differenceDrCr = (reducedCr, reducedDr) => {
    return reducedDr - reducedCr;
}
console.log(" Difference", differenceDrCr(reducedCr, reducedDr));
