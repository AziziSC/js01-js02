//JS03: Rewrite JS01 using lambda


var x = 20;
var y = 4;

var add = (a, b) => a + b;

var sub = (a, b) => a - b;

var division = (a, b) => a / b;

var mul = (a, b) => a * b;


console.log("addition", add(x, y));
console.log("subtraction", sub(x, y));
console.log("multiplication", division(x, y));
console.log("division", mul(x, y));
